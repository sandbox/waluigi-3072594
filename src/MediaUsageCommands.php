<?php

namespace Drupal\media_usage;

use Drupal\node\Entity\Node;
use Drush\Commands\DrushCommands;

/**
 * Class MediaUsageCommands.
 *
 * @package Drupal\media_usage
 */
class MediaUsageCommands extends DrushCommands {

  /**
   * Update manager service.
   *
   * @var \Drupal\media_usage\EntityUpdateManager
   */
  protected $entityUpdateManager;

  /**
   * MediaUsageCommands constructor.
   *
   * @param \Drupal\media_usage\EntityUpdateManager $entityUpdateManager
   *   Update manager service.
   */
  public function __construct(EntityUpdateManager $entityUpdateManager) {
    $this->entityUpdateManager = $entityUpdateManager;
  }

  /**
   * Deletes all entries in the reference table and sets them again.
   *
   * @description Refresh the media reference table to ensure data is up to date.
   * @command update-references
   * @aliases ur
   */
  public function updateReferences() {

    $this->entityUpdateManager->clearTable();

    // Iterate through nodes and insert entity references into database.
    $nodes = Node::loadMultiple();

    foreach ($nodes as $node) {
      $this->entityUpdateManager->trackEntityInsert($node);
      $this->output()->writeln('Update references for ' . $node->label() . ' (' . $node->id() . ')');
    }
  }

}
