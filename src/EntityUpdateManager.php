<?php

namespace Drupal\media_usage;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\node\NodeInterface;

/**
 * Class EntityUpdateManager.
 *
 * @package Drupal\media_usage
 */
class EntityUpdateManager {

  const UPDATE = 'update';
  const INSERT = 'insert';
  const DELETE = 'delete';

  /**
   * EntityUpdateManager constructor.
   */
  public function __construct() {}

  /**
   * Tracks the media usage, when an entity gets inserted.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that contains media fields.
   */
  public function trackEntityInsert(EntityInterface $entity) {
    if ($entity instanceof NodeInterface) {
      try {
        $this->recurseAndTrack($entity, self::INSERT);
      }
      catch (EntityStorageException $e) {
        watchdog_exception('media_usage', $e);
      }

    }
  }

  /**
   * Tracks the media usage, when an entity gets updated.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that contains media fields.
   */
  public function trackEntityUpdate(EntityInterface $entity) {
    if ($entity instanceof NodeInterface) {
      try {
        $this->recurseAndTrack($entity, self::UPDATE);
      }
      catch (EntityStorageException $e) {
        watchdog_exception('media_usage', $e);
      }
    }
  }

  /**
   * Tracks the media usage, when an entity gets deleted.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that contains media fields.
   */
  public function trackEntityDelete(EntityInterface $entity) {
    if ($entity instanceof NodeInterface) {
      try {
        $this->recurseAndTrack($entity, self::DELETE);
      }
      catch (EntityStorageException $e) {
        watchdog_exception('media_usage', $e);
      }
    }
  }

  /**
   * Loops through all fields and paragraph references of an entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity that should get tracked.
   * @param string $action
   *   One of insert, update or delete.
   * @param \Drupal\Core\Entity\FieldableEntityInterface $parent_entity
   *   The parent entity, in case of a recursion.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function recurseAndTrack(FieldableEntityInterface $entity, $action, FieldableEntityInterface $parent_entity = NULL) {

    if (NULL === $parent_entity) {
      $parent_entity = $entity;
    }

    foreach ($entity->getFieldDefinitions() as $definition) {

      // The field definition is a entity reference to a media entity. This is
      // where the actual tracking takes place.
      if ($definition->getItemDefinition()->getSetting('target_type') === 'media') {
        $media_entities = $entity->get($definition->getName())->referencedEntities();
        $this->trackUsage($media_entities, $parent_entity, $action);
      }

      // The field definition is a reference to a paragraph entity.
      if ($definition->getItemDefinition()->getSetting('target_type') === 'paragraph') {
        $paragraph_entities = $entity->get($definition->getName())->referencedEntities();
        foreach ($paragraph_entities as $paragraph_entity) {
          $this->recurseAndTrack($paragraph_entity, $action, $parent_entity);
        }
      }

    }
  }

  /**
   * Inserts the actual tracking to the media entity.
   *
   * @param \Drupal\media\Entity\Media[] $media_entities
   *   Entity that should get tracked by the media entities.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that contains media fields.
   * @param string $action
   *   The event to track.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function trackUsage(array $media_entities, EntityInterface $entity, $action) {
    /** @var \Drupal\media\MediaInterface $media_entity */
    foreach ($media_entities as $media_entity) {
      if ($action === self::UPDATE || $action === self::INSERT) {
        $media_entity->set('media_usage', $entity);
        $media_entity->save();
      }
      elseif ($action === self::DELETE) {
        // Remove the reference to the node from the media entity.
        $values = $media_entity->get('media_usage')->getValue();

        foreach ($values as $key => $backref) {
          if ($backref['target_id'] === $entity->id() && $backref['target_type'] === $entity->getEntityTypeId()) {
            $media_entity->get('media_usage')->removeItem($key);
          }
        }

        $media_entity->save();
      }
    }
  }

  /**
   * Deletes all entries from the table permanently.
   */
  public function clearTable() {
    try {
      $query = \Drupal::database()->delete('media__media_usage');
      $query->execute();
    }
    catch (\Exception $e) {
      watchdog_exception('media_usage', $e);
    }
  }

}
